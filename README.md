Stateless OpenPGP Command-Line Interface
========================================

This repository documents a CLI for OpenPGP that is entirely stateless.

It is currently published with the IETF at https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli

You might also want to see [the latest editor's copy](https://dkg.gitlab.io/openpgp-stateless-cli/).

The intention is to get multiple implementers to co-author it, and to get multiple implementations to exist, with different backends.

Examples of how it would be used:

```
sop generate-key "Alice Lovelace <alice@openpgp.example>" > alice.sec
sop extract-cert < alice.sec > alice.pgp

sop sign --as=text alice.sec < announcement.txt > announcement.txt.asc
sop verify announcement.txt.asc alice.pgp < announcement.txt

sop encrypt --sign-with=alice.sec bob.pgp < msg.eml > encrypted.asc
sop decrypt alice.sec < ciphertext.asc > cleartext.out
```

If you're an OpenPGP implementer, and you've built out this interface (even some subset of it) with your toolkit, please send a merge request that points to your implementation.

